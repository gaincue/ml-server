"use strict";
const Env = use('Env')
const Task = use("Task");
var Schedule = require("node-schedule");
const axios = require("axios");
const hostProxy = Env.get('HOST_PROXY', "localhost")
const port = Env.get('PORT', 3688)
const resultTime = Env.get('RESULT_TIME', "30 25 14 * * *")

class LiveResult extends Task {
  static get schedule() {
    console.log("Running cron on time :",resultTime)
    return resultTime;
  }

  async handle() {
    this.info("Task LiveResult handle");
    console.log("Running live on socket:",`http://${hostProxy}:${port}`)
    var count = 0;
    const runLive = await axios({
      method: "get",
      url:
        `http://${hostProxy}:${port}/run?api_key=b3ed4a8b-06b7-4e08-9d54-656570b736b1`,
      headers: {
        // apikey: apiKey,
        // 'Content-Type': 'application/json'
      },
    });
  }
}

module.exports = LiveResult;
