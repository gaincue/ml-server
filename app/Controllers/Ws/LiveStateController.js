'use strict'

class LiveStateController {
  constructor ({ socket, request }) {
    this.socket = socket
    this.request = request
    console.log('socket connected')
    // console.log(socket)
    // console.log(request)
  }

  onMessage (message) {
    this.socket.broadcastToAll('message', message) 
    // same as: socket.on('message')
  }

  onStatus (data) {
    this.socket.broadcastToAll('status', data) 
    // same as: socket.on('message')
  }

  onClose () {
    // same as: socket.on('close')
  }

  onError () {
    // same as: socket.on('error')
  }
}

module.exports = LiveStateController
