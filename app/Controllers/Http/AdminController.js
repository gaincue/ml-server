"use strict";
const Env = use('Env')
const User = use("App/Models/User");
const { validate } = use("Validator");
const Database = use("Database");
// const randomString = require('random-string')
// const Event = use('Event')
const Hash = use("Hash");
var moment = require("moment");
const Result = use("App/Models/Result");
const axios = require("axios");
const io = require("socket.io-client");
const hostProxy = Env.get('HOST_PROXY', "localhost")
const port = Env.get('PORT', 3688)
const socket = io(`http://${hostProxy}:${port}`);
class AdminController {
  async restartLive({ request, session, response, auth, view }) {
    //get form data
    
    try {
      console.log("INFO: ML admin restart live API is called");

      const user = await auth.user;
      if (user == null) {
        return response.redirect("/");
      }
      const todayMy = moment().add(8, "hour").format("YYYYMMDD");
      const findResult = await Result.findBy("date", todayMy);
      socket.emit("status", { date: todayMy, state: "terminate" });
      if (findResult && (findResult.status == 1 || findResult.status == 2)) {
        await findResult.delete();
        await findResult.save();
        //debug
        // const runLive = await axios({
        //   method: 'get',
        //   url: 'http://127.0.0.1:3688/run?api_key=b3ed4a8b-06b7-4e08-9d54-656570b736b1',
        // });

        //production
        const runLive = await axios({
          method: "get",
          url:
            "http://www.4dstarlive.com:3688/run?api_key=b3ed4a8b-06b7-4e08-9d54-656570b736b1"
        });
        console.log("INFO: ML admin restart live API is called successfully");
      } else {
        console.log(
          "INFO: ML admin restart live API is called without changes"
        );
      }

      return response.redirect("/admin");
    } catch (error) {
      console.log(error);
      session.flash({ error: "These credentials do not match our records." });
      return response.redirect("/");
    }
  }

  async index({ request, session, response, auth, view }) {
    //get form data
    try {
      // console.log('index home')
      const user = await auth.user;
      if (user != null) {
        return response.redirect("/admin");
      } else {
        return view.render("index");
      }
    } catch (error) {
      console.log(error);
      session.flash({ error: "These credentials do not match our records." });
      return response.redirect("/");
    }
  }

  async stopLive({ request, session, response, auth, view }) {
    //get form data
    try {
      console.log("INFO: ML admin stop live API is called");

      const user = await auth.user;
      if (user == null) {
        return response.redirect("/");
      }
      const todayMy = moment().add(8, "hour").format("YYYYMMDD");
      const findResult = await Result.findBy("date", todayMy);
      socket.emit("status", { date: todayMy, state: "terminate" });
      if (findResult && (findResult.status == 1 || findResult.status == 2)) {
        await findResult.delete();
        await findResult.save();
        console.log("INFO: ML admin stop live API is called successfully");
      } else {
        console.log("INFO: ML admin stop live API is called without changes");
      }

      return response.redirect("/admin");
    } catch (error) {
      console.log(error);
      session.flash({ error: "These credentials do not match our records." });
      return response.redirect("/");
    }
  }

  async startLive({ request, session, response, auth, view }) {
    //get form data
    try {
      console.log("INFO: ML admin start live API is called");
      const user = await auth.user;
      if (user != null) {
        const todayMy = moment().add(8, "hour").format("YYYYMMDD");
        const findResult = await Result.findBy("date", todayMy);
        if (
          findResult &&
          (findResult.status == 1 ||
            findResult.status == 2 ||
            findResult.status == 3)
        ) {
          console.log(
            "INFO: ML admin start live API is called failed as already started"
          );
        } else {
          console.log("INFO: ML admin start live API is called succesfully");
          const runLive = await axios({
            method: "get",
            url:
              "http://www.4dstarlive.com:3688/run?api_key=b3ed4a8b-06b7-4e08-9d54-656570b736b1"
          });
        }

        return response.redirect("/admin");
      } else {
        return view.render("index");
      }
    } catch (error) {
      console.log(error);
      session.flash({ error: "These credentials do not match our records." });
      return response.redirect("/");
    }
  }

  async skipLive({ request, session, response, auth, view }) {
    try {
      console.log("INFO: ML admin skip live API is called");
      const user = await auth.user;
      if (user != null) {
        const todayMy = moment().add(8, "hour").format("YYYYMMDD");
        const findResult = await Result.findBy("date", todayMy);
        if (findResult && (findResult.status == 3)) {
          console.log("INFO: Cannot started result for completed live");
        } else {
          console.log("INFO: ML admin skipped live API is called succesfully");
          socket.emit("status", { date: todayMy, state: "terminate" });
          await Result.create({ date: todayMy, status:3 });
        }
        return response.redirect("/admin");
      } else {
        return view.render("index");
      }
    } catch (error) {
      console.log(error);
      session.flash({ error: "These credentials do not match our records." });
      return response.redirect("/");
    }
  }

  async deleteResult({ request, session, response, auth, view, params }) {
    try {
      console.log("INFO: ML admin delete result API is called");
      const user = await auth.user;
      if (user != null) {
        const findResult = await Result.find(params.id);
        if (findResult) {
          await findResult.delete();
        }
        return response.redirect("/admin");
      } else {
        return view.render("index");
      }
    } catch (error) {
      console.log(error);
      session.flash({ error: "These credentials do not match our records." });
      return response.redirect("/");
    }
  }

  async home({ request, session, response, auth, view }) {
    //get form data
    try {
      // console.log('INFO: ML Admin home')
      const user = await auth.user;
      // console.log(user)
      if (user != null) {
        const findResult = await Database.select("*")
          .from("results")
          .orderBy("date", "desc")
          .limit(10);
        // console.log(findResult)
        return view.render("admin", { results: findResult, success: true });
      } else {
        return response.redirect("/");
      }
    } catch (error) {
      console.log(error);
      session.flash({ error: "These credentials do not match our records." });
      return response.redirect("/");
    }
  }

  async home({ request, session, response, auth, view }) {
    //get form data
    try {
      // console.log('INFO: ML Admin home')
      const user = await auth.user;
      // console.log(user)
      if (user != null) {
        const findResult = await Database.select("*")
          .from("results")
          .orderBy("date", "desc")
          .limit(10);
        // console.log(findResult)
        return view.render("admin", { results: findResult, success: true });
      } else {
        return response.redirect("/");
      }
    } catch (error) {
      console.log(error);
      session.flash({ error: "These credentials do not match our records." });
      return response.redirect("/");
    }
  }

  async showQRcode({ request, session, response, auth, view, params }) {
    //get form data
    try {
      // console.log('INFO: ML Show QR')
      return view.render("qr", { key: params.key, success: true });
    } catch (error) {
      console.log(error);
      session.flash({ error: "These credentials do not match our records." });
      return response.redirect("/");
    }
  }

  async login({ request, session, response, auth, view }) {
    //get form data
    try {
      console.log("login from web");
      // console.log(request.all())
      const validation = await validate(
        request.all(),
        {
          username: "required",
          password: "required"
        },
        {
          required: "The {{field}} is required.",
          unique: "The {{field}} is already exists"
        }
      );
      if (validation.fails()) {
        // console.log(validation.messages())
        // return response.status('400').send({
        //   message: validation.messages()[0].message
        // })
        session.withErrors(validation.messages()).flashAll();
        return response.redirect("back");
      }
      const { username, password } = request.all();
      const user = await auth.attempt(username, password);
      // if(!user.is_active) {
      //   // session.flash({ error: 'Login failed, current account is deactivated'})
      //   await auth.logout()
      //   return response.redirect('/')
      // }
      session.flash({ success: "Login successful" });
      console.log("login successful");
      return response.redirect("/admin");
    } catch (error) {
      console.log(error);
      // session.flash({ error: 'These credentials do not match our records.'})
      return response.redirect("/");
    }
  }

  async forgotPassword({ request, session, response, auth, view }) {
    //get form data
    try {
      console.log("forgot password");
      // console.log(user)
      return view.render("password-forgot");
    } catch (error) {
      console.log(error);
      return response.redirect("/");
    }
  }

  async logout({ auth, response }) {
    try {
      console.log("logout");
      await auth.logout();
      return response.redirect("/");
    } catch (error) {
      console.log(error);
      return response.redirect("/");
    }
  }

  async postRegister({ request, response, auth }) {
    //validate form inputs
    const validation = await validate(
      request.all(),
      {
        password: "required",
        username: "required",
        email:"required"
      },
      {
        unique: "{{ field }} is not unique",
        required: "{{ field }} is required."
      }
    );
    // show error messages upon validation fail
    if (validation.fails()) {
      return response
        .status("200")
        .send({ success: false, message: validation.messages()[0].message });
    }

    const newUser = await User.create({
      password: request.input("password"),
      username: request.input("username"),
      email: request.input("email")
    });

    return response.status("200").json({
      success: true,
      message: "Register successful",
      data: newUser
    });
  }
}

module.exports = AdminController;
