"use strict";
const Env = use("Env");
const io = require("socket.io-client");
var schedule = require("node-schedule");
const axios = require("axios");
const querystring = require("querystring");
var moment = require("moment");
const { validate } = use("Validator");
moment().format();
const Result = use("App/Models/Result");
// const getResultUrl = "http://joker.podzone.net/macau_api.php";
// const getResultUrl = "http://joker.podzone.net/newvietnam_api.php";
const host = Env.get("HOST", "localhost");
const port = Env.get("PORT", 3688);
const getAppUrl = Env.get("APP_URL", `http://${host}:${port}`);
const socketUrl = `http://localhost:${port}`;
const getResultUrl = Env.get(
  "RESULT_URL",
  "http://joker.podzone.net/macau_api.php"
);
//public results url

class LiveResultController {
  async getCurrentTime({ view, auth, response, params, request }) {
    try {
      let now = moment().tz("Asia/Singapore").unix();
      return response.status("200").send({
        success: true,
        data: now
      });
    } catch (error) {
      console.log(error);
      return response.status("400").send({
        success: false,
        data: null
      });
    }
  }

  async getResultPublic({ view, auth, response, params, request }) {
    try {
      const inputDate = params.date;
      const nowMalaysiaTime = moment()
        .add(8, "hour")
        .format("YYYY-MM-DDTHH:mm:ss");
      console.log("-INFO-API called at : " + nowMalaysiaTime);
      console.log("-INFO-Input date : " + inputDate);

      var jppoolEmpty = 0.0;
      var jpbonusEmpty = {
        first_prize: { unit: 0, number: 0, percent: 0, prize: 0 },
        second_prize: { unit: 0, number: 0, percent: 0, prize: 0 },
        third_prize: { unit: 0, number: 0, percent: 0, prize: 0 }
      };

      var jppoolPrev = 0;
      var jpbonusPrev = {
        first_prize: { unit: 0, number: 0, percent: 0, prize: 0 },
        second_prize: { unit: 0, number: 0, percent: 0, prize: 0 },
        third_prize: { unit: 0, number: 0, percent: 0, prize: 0 }
      };
      //fixes for malaysia time +8 GMT
      const todayMy = moment().add(8, "hour").format("YYYYMMDD");
      if (inputDate == todayMy) {
        var startdate = moment(todayMy).subtract(1, "d").format("YYYYMMDD");
        console.log("Yesterday: " + startdate);
        const getPrevResults = await axios({
          method: "post",
          url: "http://adm.mlhk99.com/mlhk99/graphicapi.php",
          headers: { "content-type": "application/x-www-form-urlencoded" },
          // data: bodyFormData
          data: querystring.stringify({
            logn: "9%2A%5BTK%40B%5C",
            logp: "6z%25d%25%2F3%23~%20",
            ldate: startdate
          })
        });
        if (getPrevResults.data.success == true) {
          //prevent null data from ytd
          if (
            getPrevResults.data.results.jpbonus.first_prize.unit != null &&
            getPrevResults.data.results.jpbonus.second_prize.unit != null &&
            getPrevResults.data.results.jpbonus.third_prize.unit != null
          ) {
            jpbonusPrev = getPrevResults.data.results.jpbonus;
          }
          if (getPrevResults.data.results.jppool != null) {
            jppoolPrev = getPrevResults.data.results.jppool;
          }
        }
      }

      const getResults = await axios({
        method: "post",
        url: "http://adm.mlhk99.com/mlhk99/graphicapi.php",
        headers: { "content-type": "application/x-www-form-urlencoded" },
        // data: bodyFormData
        data: querystring.stringify({
          logn: "9%2A%5BTK%40B%5C",
          logp: "6z%25d%25%2F3%23~%20",
          ldate: inputDate
        })
      });
      console.log("DEBUG: get results = ");
      console.log(getResults.data);
      // console.log(getResults.data.results.jpbonus.first_prize)
      // console.log(getResults.data.results.jpbonus.second_prize)
      // console.log(getResults.data.results.jpbonus.third_prize)

      if (getResults.data.success == false) {
        // console.log("DEBUG: get results false and return previous jppoll result only")

        return view.render("result", {
          date: inputDate,
          results: {
            jppool: jppoolPrev,
            jpbonus: jpbonusPrev
          }
        });

        // return response.status('200').send({
        //   success: false,
        //   results: {
        //     jppool: jppoolPrev,
        //     jpbonus: jpbonusPrev
        //   }
        // })
      }
      // console.log(getResults.data)
      //check live
      const checkDate = await Result.findBy("date", inputDate);
      try {
        console.log("DEBUG: Check date = " + checkDate.status);
      } catch (error) {
        console.log("DEBUG: Check date failed, still in live?");
      }

      try {
        if (getResults.data.success == true && checkDate.status != 3) {
          // console.log("DEBUG: Get results failed condition 1 : " + inputDate)

          return view.render("result", {
            date: inputDate,
            results: {
              jppool: getResults.data.results.jppool
            }
          });

          // return response.status('200').send({
          //   success: false,
          //   results: {
          //     jppool: getResults.data.results.jppool
          //   }
          // })
        }
      } catch (error) {
        console.log(error);
        // console.log("DEBUG: Get results failed condition 2 : " + inputDate)

        return view.render("result", {
          date: inputDate,
          results: {
            jppool: getResults.data.results.jppool
          }
        });
      }
      // console.log(getResults.data)
      var data = getResults.data.results;
      const filter = ["first_prize", "second_prize", "third_prize"];
      var seq = [];
      var seq4d = data["4D"]["sequence"];
      for (var i in seq4d) {
        var current = seq4d[i];
        var isValid = true;
        for (var x in filter) {
          if (current == data["4D"][filter[x]][0]) {
            isValid = false;
          }
        }
        if (isValid) {
          seq.push(current);
        }
      }

      //post process
      for (var i in data["4D"]["special_prize"]) {
        var selVal = data["4D"]["special_prize"][i];
        if (
          selVal == data["4D"]["first_prize"] ||
          selVal == data["4D"]["second_prize"] ||
          selVal == data["4D"]["third_prize"]
        ) {
          data["4D"]["special_prize"][i] =
            data["4D"]["special_prize"][i][0] + " ----";
          data["jackpot"]["special_prize"][i] = "------";
        }
      }

      for (var i in data["4D"]["consolation"]) {
        var selVal = data["4D"]["consolation"][i];
        if (
          selVal == data["4D"]["first_prize"] ||
          selVal == data["4D"]["second_prize"] ||
          selVal == data["4D"]["third_prize"]
        ) {
          data["4D"]["consolation"][i] =
            data["4D"]["consolation"][i][0] + " ----";
          data["jackpot"]["consolation"][i] = "------";
        }
      }

      if (
        data["jpbonus"]["first_prize"]["number"] == null &&
        data["jpbonus"]["second_prize"]["number"] == null &&
        data["jpbonus"]["third_prize"]["number"] == null
      ) {
        data["jpbonus"] = jpbonusEmpty;
      }
      if (data["jppool"] == null) {
        data["jppool"] = jppoolEmpty;
      }
      data["jackpot"]["sequence"] = seq;

      // console.log('DEBUG: Get results successfully and returned data')
      // console.log(data)
      //post process for jp bonus string
      var bonusType = ["NO BONUS", "SMALL BONUS", "BIG BONUS", "SUPER BONUS"];
      var fpPercent = 0.0,
        spPercent = 0.0,
        tpPercent = 0.0;
      var fpPrize = 0.0,
        spPrize = 0.0,
        tpPrize = 0.0;
      var fpUnit = 0,
        spUnit = 0,
        tpUnit = 0;
      if (data["jpbonus"]["first_prize"]["percent"]) {
        fpPercent = parseFloat(data["jpbonus"]["first_prize"]["percent"]);
      }
      if (data["jpbonus"]["second_prize"]["percent"]) {
        spPercent = parseFloat(data["jpbonus"]["second_prize"]["percent"]);
      }
      if (data["jpbonus"]["third_prize"]["percent"]) {
        tpPercent = parseFloat(data["jpbonus"]["third_prize"]["percent"]);
      }
      if (data["jpbonus"]["first_prize"]["prize"]) {
        fpPrize = parseFloat(data["jpbonus"]["first_prize"]["prize"]);
      }
      if (data["jpbonus"]["second_prize"]["prize"]) {
        spPrize = parseFloat(data["jpbonus"]["second_prize"]["prize"]);
      }
      if (data["jpbonus"]["third_prize"]["prize"]) {
        tpPrize = parseFloat(data["jpbonus"]["third_prize"]["prize"]);
      }
      if (data["jpbonus"]["first_prize"]["unit"]) {
        fpUnit = parseFloat(data["jpbonus"]["first_prize"]["unit"]);
      }
      if (data["jpbonus"]["second_prize"]["unit"]) {
        spUnit = parseFloat(data["jpbonus"]["second_prize"]["unit"]);
      }
      if (data["jpbonus"]["third_prize"]["unit"]) {
        tpUnit = parseFloat(data["jpbonus"]["third_prize"]["unit"]);
      }

      var bonusType = bonusType[0];
      if (fpPrize > 0) {
        bonusType = bonusType[4];
      } else if (spPrize > 0) {
        bonusType = bonusType[3];
      } else if (tpPrize > 0) {
        bonusType = bonusType[2];
      }
      var pool = parseFloat(data["jppool"]);
      var jpbonus1 =
        "First Prize " +
        data["jpbonus"]["first_prize"]["number"] +
        " $" +
        fpPrize +
        "/unit";
      var jpbonus2 =
        "Second Prize " +
        data["jpbonus"]["second_prize"]["number"] +
        " $" +
        spPrize +
        "/unit";
      var jpbonus3 =
        "Third Prize " +
        data["jpbonus"]["third_prize"]["number"] +
        " $" +
        tpPrize +
        "/unit";

      data["jpbonus"] = {
        type: bonusType,
        first_prize: jpbonus1,
        second_prize: jpbonus2,
        third_prize: jpbonus3,
        jpbonus4: "",
        jpbonus5: "",
        jpbonus6: ""
      };

      var jptotal1 = [];
      var jptotal2 = [];
      var jptotal3 = [];
      var total = (pool * fpPercent) / 100;
      jptotal1.push("Number of units : " + fpUnit);
      jptotal1.push("$" + total + " = $" + pool + " x " + fpPercent + "%");
      jptotal1.push("$" + fpPrize + " = $" + total + " / " + fpUnit);
      data["jpbonus"]["jpbonus4"] = jptotal1;

      if (spUnit > 0 && spPercent > 0 && spPrize > 0) {
        var total = (pool * spPercent) / 100;
        jptotal2.push("Number of units : " + spUnit);
        jptotal2.push("$" + total + " = $" + pool + " x " + spPercent + "%");
        jptotal2.push("$" + spPrize + " = $" + total + " / " + spUnit);
        data["jpbonus"]["jpbonus5"] = jptotal2;
      }

      if (tpUnit > 0 && tpPercent > 0 && tpPrize > 0) {
        var total = (pool * tpPercent) / 100;
        jptotal3.push("Number of units : " + tpUnit);
        jptotal3.push("$" + total + " = $" + pool + " x " + tpPercent + "%");
        jptotal3.push("$" + tpPrize + " = $" + total + " / " + tpUnit);
        data["jpbonus"]["jpbonus6"] = jptotal3;
      }

      return view.render("result", { date: inputDate, results: data });
    } catch (error) {
      console.log(error);
      // if (inputDate) {
      //   console.log("ERROR: Get results failed condition 3 : " + inputDate)
      // }
      // return response.status('400').send({
      //   "success": false,
      //   "results": null
      // })
      return view.render("result", {
        date: inputDate,
        results: null,
        message: "Failed to get result"
      });
    }
  }

  async getResult({ view, auth, response, params, request }) {
    try {
      const inputDate = request.input("input_date");
      const validation = await validate(
        request.all(),
        {
          input_date: "required"
        },
        {
          required: "The {{field}} is required.",
          unique: "The {{field}} is already exists"
        }
      );
      if (validation.fails()) {
        console.log(validation.messages()[0].message);
        return response
          .status("400")
          .send({ message: validation.messages()[0].message });
      }
      const nowMalaysiaTime = moment()
        .add(8, "hour")
        .format("YYYY-MM-DDTHH:mm:ss");
      console.log("-INFO-API called at : " + nowMalaysiaTime);
      console.log("-INFO-Input date : " + inputDate);

      var jppoolEmpty = 0.0;
      var jppoolPrev = 0;
      //fixes for malaysia time +8 GMT
      const todayMy = moment().add(8, "hour").format("YYYYMMDD");
      if (inputDate == todayMy) {
        var startdate = moment(todayMy).subtract(1, "d").format("YYYYMMDD");
        console.log("Yesterday: " + startdate);
        const getPrevResults = await axios({
          method: "post",
          url: getResultUrl + "?date=" + startdate,
          headers: { "content-type": "application/x-www-form-urlencoded" }
          // data: bodyFormData
          // data: querystring.stringify({
          //   "logn":'9%2A%5BTK%40B%5C',
          //   "logp":'6z%25d%25%2F3%23~%20',
          //   "ldate": startdate
          // }),
        });
        // console.log("DEBUG: yesterday get result: ")
        // console.log(getPrevResults)
        if (getPrevResults.data.success == true) {
          //prevent null data from ytd
          if (getPrevResults.data.jackpot.first_prize_winner_count != null) {
            jppoolPrev = getPrevResults.data.jackpot.first_prize_winner_count;
          }
        }
      }

      const getResults = await axios({
        method: "post",
        url: getResultUrl + "?date=" + inputDate,
        headers: { "content-type": "application/x-www-form-urlencoded" }
        // data: bodyFormData
        // data: querystring.stringify({
        //   "logn":'9%2A%5BTK%40B%5C',
        //   "logp":'6z%25d%25%2F3%23~%20',
        //   "ldate": inputDate
        // }),
      });
      // console.log("DEBUG: get results = ");
      // console.log(getResults.data.success);
      // console.log(getResults.data.results.jpbonus.first_prize)
      // console.log(getResults.data.results.jpbonus.second_prize)
      // console.log(getResults.data.results.jpbonus.third_prize)

      if (getResults.data.success == false) {
        // console.log("DEBUG: get results false and return previous jppoll result only")
        return response.status("200").send({
          success: false,
          results: {
            jppool: jppoolPrev
          }
        });
      }
      // console.log(getResults.data)
      //check live
      const checkDate = await Result.findBy("date", inputDate);
      // try {
      //   // console.log('DEBUG: Check date = ' + checkDate.status)
      // } catch (error) {
      //   console.log("DEBUG: Check date failed, still in live?");
      // }
      if (checkDate == null) {
        return response.status("200").send({
          success: false,
          results: {
            jppool: jppoolPrev
          }
        });
      }

      try {
        if (getResults.data.success == true && checkDate.status != 3) {
          // console.log("DEBUG: Get results failed condition 1 : " + inputDate);
          return response.status("200").send({
            success: false,
            results: {
              jppool: getResults.data.results.jppool,
              error: "1"
            }
          });
        }
      } catch (error) {
        console.log(error);
        // console.log("DEBUG: Get results failed condition 2 : " + inputDate);
        return response.status("200").send({
          success: false,
          results: {
            jppool: getResults.data.results.jppool,
            error: "2"
          }
        });
      }
      // console.log(getResults.data)
      var data = getResults.data.results;
      data["jackpot"] = getResults.data.jackpot;
      const filter = ["first_prize", "second_prize", "third_prize"];
      const filterPrize = [
        "first_prize",
        "second_prize",
        "third_prize",
        "special_prize"
      ];
      const fourD = data["4D"];
      let jackpotWinner = "";
      for (var cnt in filterPrize) {
        let current = "";
        if (filterPrize[cnt] == "special_prize") {
          current = data["4D"]["special_prize"];
          for (var i in current) {
            const inner = current[i];
            if (inner[0] == data["jackpot"]["alpha"]) {
              jackpotWinner = inner;
            }
          }
        } else {
          current = data["4D"][filterPrize[cnt]];
          if (current[0] == data["jackpot"]["alpha"]) {
            jackpotWinner = current;
          }
        }
      }
      // var seq = [];
      // var seq4d = data["4D"]["sequence"];
      // for (var i in seq4d) {
      //   var current = seq4d[i];
      //   var isValid = true;
      //   for (var x in filter) {
      //     if (current == data["4D"][filter[x]][0]) {
      //       isValid = false;
      //     }
      //   }
      //   if (isValid) {
      //     seq.push(current);
      //   }
      // }
      // console.log(data)
      if (data["jackpot"]["first_prize_winner_count"] == null) {
        data["jackpot"] = jpbonusEmpty;
        data["jppool"] = jppoolEmpty;
      } else {
        data["jppool"] = data["jackpot"]["first_prize_money"];
        data["jackpot"]["winner"] = jackpotWinner;
      }
      // console.log("DEBUG: Get results successfully and returned data");
      // console.log(data)
      return response.status("200").send({
        success: true,
        results: data
      });
    } catch (error) {
      console.log(error);
      // console.log("ERROR: Get results failed condition 3 : " + inputDate);
      return response.status("400").send({
        success: false,
        results: null
      });
    }
  }
  // https://soso.selfip.com/soonzhi/get_result.php?date=${inputDate}&game=${gameNum}`
  async getAlternativeResultv2({ view, auth, response, params, request }) {
    try {
      const inputDate = request.input("input_date");
      const validation = await validate(
        request.all(),
        {
          input_date: "required",
          game: "required"
        },
        {
          required: "The {{field}} is required.",
          unique: "The {{field}} is already exists"
        }
      );
      if (validation.fails()) {
        console.log(validation.messages()[0].message);
        return response
          .status("400")
          .send({ message: validation.messages()[0].message });
      }
      console.log("input date : " + inputDate);
      //post axios to get result
      const game = request.input("game");
      const gameMap = {
        magnum: 1,
        damacai: 2,
        toto: 3,
        singapore: 4,
        cash: 5,
        sabah: 6,
        stc: 7,
        ml: 8,
        vietnam: 9,
        singapore_toto: 41,
        test_magnum: 1,
        test_toto: 3
      };

      const gameLiveTime = {
        magnum: [
          ["wed", "sat", "sun"],
          ["19:00", "20:00"]
        ],
        damacai: [
          ["wed", "sat", "sun"],
          ["19:00", "20:00"]
        ],
        toto: [
          ["wed", "sat", "sun"],
          ["19:00", "20:00"]
        ],
        singapore: [
          ["wed", "sat", "sun"],
          ["18:30", "19:30"]
        ],
        cash: [
          ["wed", "sat", "sun"],
          ["19:00", "20:00"]
        ],
        sabah: [
          ["wed", "sat", "sun"],
          ["19:00", "20:00"]
        ],
        stc: [
          ["wed", "sat", "sun"],
          ["19:00", "20:00"]
        ],
        ml: [
          ["mon", "tue", "wed", "thu", "fri", "sat", "sun"],
          ["20:25", "23:35"]
        ],
        vietnam: [
          ["mon", "tue", "wed", "thu", "fri", "sat", "sun"],
          ["19:20", "21:00"]
        ],
        singapore_toto: [
          ["wed", "sat", "sun"],
          ["19:00", "21:00"]
        ],
        test_magnum: [
          ["mon", "tue", "wed", "thu", "fri", "sat", "sun"],
          ["19:00", "22:00"]
        ],
        test_toto: [
          ["mon", "tue", "wed", "thu", "fri", "sat", "sun"],
          ["12:00", "23:50"]
        ]
      };
      let getResults = {};
      if (Number(gameMap[game]) <= 7 || Number(gameMap[game]) == 41) {
        getResults = await axios({
          method: "get",
          url: `https://soso.selfip.com/soonzhi/get_result.php?date=${inputDate}&game=${gameMap[game]}`
          // headers: { "content-type": "application/x-www-form-urlencoded" },
          // data: bodyFormData
        });
      } else if (game == "ml") {
        //vietnam or ml
        getResults = await axios({
          method: "get",
          url: `http://joker.podzone.net/macau_api.php?date=${inputDate}`
        });
      } else if (game == "vietnam") {
        getResults = await axios({
          method: "get",
          url: `http://joker.podzone.net/newvietnam_api.php?date=${inputDate}`
        });
      }

      // console.log(getResults);
      if (getResults == null || getResults.data.success == false) {
        return response.status("200").send({
          success: false,
          results: {}
        });
      }
      var data = {};
      // console.log(data)
      // const gameList = [
      //   "magnum",
      //   "damacai",
      //   "toto",
      //   "cash",
      //   "sabah",
      //   "stc",
      //   "singapore"
      // ];
      // console.log(getResults.data)
      // console.log(data);
      getResults.data.results["game"] = getResults.data.name;
      if (getResults.data.jackpot != null) {
        getResults.data.results["jackpot"] = getResults.data.jackpot;
      }

      getResults.data.results["is_live"] = false;
      getResults.data.results["is_today"] = false;
      getResults.data.results["date_started"] = "";

      var getInputDate = moment(inputDate);
      var weekDayName = moment(inputDate).format("ddd");
      console.log(getInputDate.format(), weekDayName);
      if (
        moment(inputDate).format("YYYYMMDD") == moment().format("YYYYMMDD") &&
        gameLiveTime[game][0].includes(weekDayName.toLowerCase())
      ) {
        console.log("today matched");
        const fromTime = moment(inputDate)
          .add(gameLiveTime[game][1][0].split(":")[0], "hours")
          .add(gameLiveTime[game][1][0].split(":")[1], "minutes");

        const toTime = moment(inputDate)
          .add(gameLiveTime[game][1][1].split(":")[0], "hours")
          .add(gameLiveTime[game][1][1].split(":")[1], "minutes");

        getResults.data.results["date_started"] = fromTime.format();
        console.log(moment(inputDate).format(""));
        console.log(gameLiveTime[game][1][0].split(":")[0]);
        console.log(gameLiveTime[game][1][0].split(":")[1]);
        console.log(gameLiveTime[game][1][1].split(":")[0]);
        console.log(gameLiveTime[game][1][1].split(":")[1]);
        const currentDateTime = moment().add(8, "hours").format();
        console.log("current_date", currentDateTime);
        console.log("from time : ", fromTime.format());
        console.log("to time: ", toTime.format());
        if (
          currentDateTime >= fromTime.format() &&
          currentDateTime <= toTime.format()
        ) {
          console.log("live matched");
          getResults.data.results["is_live"] = true;
        }
        console.log("2 from time: ", currentDateTime);
        console.log("2 to time: ", toTime.format());
        if (currentDateTime >= toTime.format()) {
          console.log("live finish matched");
          getResults.data.results["is_today"] = true;
          getResults.data.results["is_live"] = false;
          getResults.data.results["date_started"] = "";
        }
      }

      if (game == "test_magnum") {
        const nowDate = new Date();
        getResults.data.results["is_live"] = true;
        getResults.data.results["is_today"] = true;
        getResults.data.results["date_started"] = nowDate.valueOf();
      }

      return response.status("200").send({
        success: true,
        results: getResults.data.results
      });
    } catch (error) {
      console.log(error);
      return response.status("400").send({
        success: false,
        results: {}
      });
    }
  }

  async getAlternativeResult({ view, auth, response, params, request }) {
    try {
      const inputDate = request.input("input_date");
      const validation = await validate(
        request.all(),
        {
          input_date: "required",
          game: "required"
        },
        {
          required: "The {{field}} is required.",
          unique: "The {{field}} is already exists"
        }
      );
      if (validation.fails()) {
        console.log(validation.messages()[0].message);
        return response
          .status("400")
          .send({ message: validation.messages()[0].message });
      }
      console.log("input date : " + inputDate);
      // var getResults =
      // {
      //   "data" :{
      //   //extra prizes
      //     "success":true,
      //     "data": {
      //       "magnum":{
      //           //delete repeat number in special
      //           //same as macau lotto
      //           "date":"20190105",
      //           "first_prize":"E7464",
      //           "second_prize":"C1738",
      //           "third_prize":"D4293",
      //           "special_prize":["A4742","B9194","C1738","D4293","E7464","F5748","G4900"],
      //           "consolation_prize":["N4418","P5809","Q5286","R7854","S2222","T6981","U3481","V2926","W4745"],
      //       },
      //       "damacai":{
      //           "date":"20190105",
      //           "first_prize":"0292",
      //           "second_prize":"7884",
      //           "third_prize":"5728",
      //           "special_prize":["4742","9194","5748","4900","8168","9635","8690","1100","4880","6266"],
      //           "consolation_prize":["4418","8294","5809","5286","7854","2222","6981","3481","2926","4745"],
      //       },
      //       "toto":{
      //           //same as macau lotto
      //           "date":"20190105",
      //           "first_prize":"E7464",
      //           "second_prize":"C1738",
      //           "third_prize":"D4293",
      //           "special_prize":["A4742","B9194","C1738","D4293","E7464","F5748","G4900","H8168","I9635","J8690","K1100","L4880","M6266"],
      //           "consolation_prize":["N4418","O8294","P5809","Q5286","R7854","S2222","T6981","U3481","V2926","W4745"],
      //       },
      //       "sweep":{
      //           "date":"20190105",
      //           "first_prize":"0292",
      //           "second_prize":"7884",
      //           "third_prize":"5728",
      //           "special_prize":["4742","9194","1886","4900","8168","9635","8690","1100","4880","6266"],
      //           "consolation_prize":["4418","8294","5809","5286","7854","2222","6981","3481","2926","4745"],
      //       },
      //       "sabah":{
      //           //same as macau lotto
      //           "date":"20190105",
      //           "first_prize":"0292",
      //           "second_prize":"7884",
      //           "third_prize":"5728",
      //           "special_prize":["4742","9194","7884","5728","0292","5748","4900","8168","9635","8690","1100","4880","6266"],
      //           "consolation_prize":["4418","8294","5809","5286","7854","2222","6981","3481","2926","4745"],
      //       },
      //       "stc":{
      //           //same as macau lotto
      //           "date":"20190105",
      //           "first_prize":"0292",
      //           "second_prize":"7884",
      //           "third_prize":"5728",
      //           "special_prize":["4742","9194","7884","5728","0292","5748","4900","8168","9635","8690","1100","4880","6266"],
      //           "consolation_prize":["4418","8294","5809","5286","7854","2222","6981","3481","2926","4745"],
      //       },
      //       "singapore":{
      //           "date":"20190105",
      //           "first_prize":"0292",
      //           "second_prize":"7884",
      //           "third_prize":"5728",
      //           "special_prize":["4742","9194","4354","4900","8168","9635","8690","1100","4880","6266"],
      //           "consolation_prize":["4418","8294","5809","5286","7854","2222","6981","3481","2926","4745"],
      //       }
      //     }
      //   }
      // }

      //post axios to get result
      const getResults = await axios({
        method: "post",
        url: "http://result.mlhk99.com/sevens/sevensapi.php",
        headers: { "content-type": "application/x-www-form-urlencoded" },
        // data: bodyFormData
        data: querystring.stringify({
          logn: "9%2A%5BTK%40B%5C",
          logp: "6z%25d%25%2F3%23~%20",
          ldate: inputDate
        })
      });
      // console.log(getResults)
      if (getResults.data.success == false) {
        return response.status("400").send({
          success: false,
          results: null
        });
      }
      // var rgame = request.input('game')
      var data = {};

      data = {
        date: "",
        first_prize: "",
        second_prize: "",
        third_prize: "",
        special_prize: [],
        consolation_prize: []
      };
      // console.log(data)
      const gameList = [
        "magnum",
        "damacai",
        "toto",
        "cash",
        "sabah",
        "stc",
        "singapore"
      ];
      // console.log(getResults.data)
      try {
        var rdata = getResults.data.data[request.input("game")];
        // console.log("rdata",rdata)
        data.date = rdata.date;
        var stemp = [];
        for (var scount = 0; scount < rdata.special_prize.length; scount++) {
          stemp.push(rdata.special_prize[scount]);
        }

        for (var scount = rdata.special_prize.length; scount < 10; scount++) {
          stemp.push("");
        }
        data.special_prize = stemp;
        var ctemp = [];
        for (
          var ccount = 0;
          ccount < rdata.consolation_prize.length;
          ccount++
        ) {
          ctemp.push(rdata.consolation_prize[ccount]);
        }

        for (
          var ccount = rdata.consolation_prize.length;
          ccount < 10;
          ccount++
        ) {
          ctemp.push("");
        }
        data.consolation_prize = ctemp;

        if (rdata.third_prize.length != 0) {
          data.third_prize = rdata.third_prize;
        }
        if (rdata.second_prize.length != 0) {
          data.second_prize = rdata.second_prize;
        }
        if (rdata.first_prize.length != 0) {
          data.first_prize = rdata.first_prize;
        }
        if (request.input("game") == "toto") {
          data.first_prize_5d = rdata.first_prize_5d;
          data.second_prize_5d = rdata.second_prize_5d;
          data.third_prize_5d = rdata.third_prize_5d;
          data.first_prize_6d = rdata.first_prize_6d;
        }
        // console.log(data);
      } catch (error) {
        console.log(error);
        return response.status("400").send({
          success: false,
          results: null
        });
      }
      // var data = getResults.data.data
      return response.status("200").send({
        success: true,
        results: data
      });
    } catch (error) {
      console.log(error);
      return response.status("400").send({
        success: false,
        results: null
      });
    }
  }

  async playLiveResult({ view, auth, response, params, request }) {
    try {
      console.log("Running live on socket:", socketUrl);
      var socket = io(socketUrl);
      var input = {
        success: true,
        results: {
          "4D": {
            date: "20190105",
            first_prize: "E7464",
            second_prize: "C1738",
            third_prize: "D4293",
            special_prize: [
              "A4742",
              "B9194",
              "C1738",
              "D4293",
              "E7464",
              "F5748",
              "G4900",
              "H8168",
              "I9635",
              "J8690",
              "K1100",
              "L4880",
              "M6266"
            ],
            consolation_prize: [
              "N4418",
              "O8294",
              "P5809",
              "Q5286",
              "R7854",
              "S2222",
              "T6981",
              "U3481",
              "V2926",
              "W4745"
            ],
            sequence: [
              "M",
              "N",
              "U",
              "D",
              "E",
              "F",
              "G",
              "H",
              "I",
              "J",
              "K",
              "L",
              "A",
              "B",
              "O",
              "P",
              "Q",
              "R",
              "S",
              "T",
              "C",
              "V",
              "W"
            ]
          },
          "5D": {
            date: "20190105",
            first_prize: "39821",
            second_prize: "92668",
            third_prize: "57324"
          },
          "6D": { date: "20190105", first_prize: "216824" },
          jackpot: {
            date: "20200216",
            alpha: "D",
            first_prize_money: 21747.7,
            first_prize_winner_count: 0.0,
            second_prize_money: 10873.85,
            second_prize_winner_count: 0.0,
            third_prize_money: 6524.31,
            third_prize_winner_count: 0.0,
            special_prize_money: 1000.0,
            special_prize_winner_count: 0.0
          },
          jppool: 100000,
          jpbonus: {}
        }
      };
      var input = "";
      //.subtract(1, 'days')
      let now = moment().add(8, "hour").format("YYYYMMDD");
      if (request.input("override_date") != null) {
        now = request.input("override_date");
      }
      const checkLive = await Result.findBy("date", now);
      if (!checkLive) {
        const newLive = await Result.create({
          date: now,
          status: 1
        });
      } else {
        console.log("WARNING: Live socket already started : " + now);
        return response.status("200").send({
          message: "live socket already started",
          success: false
        });
      }

      const getLive = await Result.findBy("date", now);
      console.log("INFO: Play live socket results : " + now);
      let startTime = new Date(Date.now());
      let endTime = new Date(startTime.getTime() + 5000000);
      var count = 0;
      var gamePerActionDuration = 0;
      var gamePauseDuration = 0;
      var game = "4D";
      var selection = "";
      var gameSelected = "";
      var gameCount = 0;
      var gameSize = 9;
      var actionCount = 0;
      var actionSize = 0;
      var state = "NEW_GAME";
      var result = null;
      var sequence = null;
      var actionData = null;
      var storedData = [];
      var selectedSeq = 0;
      var seqState = 0;
      var isStoreResult = false;
      var prevState = 0;
      var isResultReady = false;
      var storedResults = {
        "4D": {
          first_prize: "",
          second_prize: "",
          third_prize: "",
          special_prize: [],
          consolation_prize: [],
          sequence: []
        },
        "5D": { first_prize: "", second_prize: "", third_prize: "" },
        "6D": { first_prize: "" },
        jackpot: {
          // "first_prize":"",
          // "second_prize":"",
          // "third_prize":"",
          // "special_prize":[],
          // "consolation_prize":[],
          // "jppool":"0",
          // "jpbonus":{},
          // "winner":[[3,"132650",80000,5],[3,"132650",80000,5]]
          alpha: "",
          first_prize_money: 0.0,
          first_prize_winner_count: 0.0,
          second_prize_money: 0.0,
          second_prize_winner_count: 0.0,
          third_prize_money: 0.0,
          third_prize_winner_count: 0.0,
          special_prize_money: 0.0,
          special_prize_winner_count: 0.0
        },
        jppool: "0"
      };
      var liveResultThread = schedule.scheduleJob(
        { start: startTime, end: endTime, rule: "*/1 * * * * *" },
        async function () {
          /*game count
          0 - 4d consolation
          1 - 4d special_prize
          2 - 4d third
          3 - 4d second
          4 - 4d first
          5 - 5d third
          6 - 5d second
          7 - 5d first
          8 - 6d first
        */
          console.log("Time count : " + count);

          socket.on("status", (data) => {
            var receiveData = JSON.parse(JSON.stringify(data));
            // console.log('controller received state: ' + receiveData.state)
            if (receiveData.state == "terminate") {
              console.log("controller received state: " + receiveData.state);
              liveResultThread.cancel();
              state = "END";
              return;
            }
          });

          if (state == "NEW_GAME") {
            //send a static page
            // count = 600
            socket.emit("status", { date: now, state: "static" });
            count = 0;
            state = "WAIT_RESULT";
          }
          if (state == "WAIT_RESULT") {
            //wait for 10 minutes for result
            //or query for data, for 10 minutes
            if (count < 30) {
              socket.emit("status", { date: now, state: "static" });
            } else {
              state = "QUERY_RESULT";
              count = 0;
            }
          }
          if (state == "QUERY_RESULT") {
            if (count < 10) {
              socket.emit("status", { date: now, state: "static" });
            } else {
              const queryResults = await axios({
                method: "post",
                url: getResultUrl + "?date=" + now,
                headers: { "content-type": "application/x-www-form-urlencoded" }
                // data: bodyFormData
                // data: querystring.stringify({
                //   "logn":'9%2A%5BTK%40B%5C',
                //   "logp":'6z%25d%25%2F3%23~%20',
                //   "ldate": now
                // }),
              });
              console.log("INFO: Get results for live = ");
              console.log(queryResults.data);
              const getResults = queryResults.data;
              // console.log(getResults.data.results.jpbonus.first_prize)
              // console.log(getResults.data.results.jpbonus.second_prize)
              // console.log(getResults.data.results.jpbonus.third_prize)
              // console.log(getResults.data.success)
              if (getResults) {
                if (getResults.success == true) {
                  isResultReady = true;
                  input = getResults;
                  storedResults.jppool = input["jackpot"]["first_prize_money"];
                }
                //FIX: today jpool and jpbonus now showing
                // storedResults.jpbonus = input['results']['jpbonus']
              }
              count = 0;
            }
            if (isResultReady) {
              // socket.emit('status',{date:now,state:'live'})
              getLive.status = 2;
              await getLive.save();
              state = "PREPARE_GAME";
            }
          }
          if (state == "PREPARE_GAME") {
            console.log("-DEBUG-game count: " + gameCount);
            if (gameCount == 0) {
              result = "INTRO";
              selection = "introduction";
              actionSize = 1;
              game = "INTRO";
              gamePauseDuration = 5;
              gamePerActionDuration = 5;
            }
            if (gameCount == 1) {
              result = input["results"]["4D"];
              selection = "sequence";
              game = "4D";
              actionSize = result[selection].length;
              gamePerActionDuration = 0;
              gamePauseDuration = 5;
              seqState = 0;
            } else if (gameCount == 2) {
              result = input["results"]["4D"];
              game = "4D";
              selection = "third_prize";
              gameSelected = selection;
              actionSize = 1;
              gamePauseDuration = 5;
              gamePerActionDuration = 25;
            } else if (gameCount == 3) {
              result = input["results"]["4D"];
              selection = "second_prize";
              gameSelected = selection;
              game = "4D";
              actionSize = 1;
              gamePauseDuration = 5;
              gamePerActionDuration = 10;
            } else if (gameCount == 4) {
              result = input["results"]["4D"];
              selection = "first_prize";
              gameSelected = selection;
              game = "4D";
              actionSize = 1;
              gamePauseDuration = 5;
              gamePerActionDuration = 10;
            } else if (gameCount == 5) {
              result = input["results"]["5D"];
              selection = "third_prize";
              gameSelected = selection;
              game = "5D";
              actionSize = 1;
              gamePauseDuration = 5;
              gamePerActionDuration = 50;
            } else if (gameCount == 6) {
              result = input["results"]["5D"];
              selection = "second_prize";
              gameSelected = selection;
              game = "5D";
              actionSize = 1;
              gamePauseDuration = 5;
              gamePerActionDuration = 50;
            } else if (gameCount == 7) {
              result = input["results"]["5D"];
              selection = "first_prize";
              gameSelected = selection;
              game = "5D";
              actionSize = 1;
              gamePauseDuration = 5;
              gamePerActionDuration = 50;
            } else if (gameCount == 8) {
              result = input["results"]["6D"];
              selection = "first_prize";
              gameSelected = selection;
              game = "6D";
              actionSize = 1;
              gamePauseDuration = 5;
              gamePerActionDuration = 60;
            } else if (gameCount == 9) {
              result = "OUTRO";
              selection = "outro";
              gameSelected = selection;
              game = "OUTRO";
              actionSize = 1;
              gamePerActionDuration = 5;
              gamePauseDuration = 2;
              storedResults.jackpot = input["jackpot"];
              storedResults.jppool = input["jackpot"]["first_prize_money"];
            }
            actionCount = 0;
            // count = gamePerActionDuration
            state = "LOAD_GAME";
          }
          if (state == "LOAD_GAME") {
            if (count > gamePauseDuration) {
              state = "LOOP_NUMBER";
              count = gamePerActionDuration;

              socket.emit("status", { state: "loop_number", game: game });
            } else if (game == "INTRO" || game == "OUTRO") {
              state = "LOOP_NUMBER";
              count = 0;
            } else {
              // socket.emit('status',{state:'load',})
            }
          }
          if (state == "LOOP_NUMBER") {
            if (count == gamePerActionDuration) {
              console.log("-DEBUG-count hit game per duration");
              console.log("-DEBUG-game:" + game);
              console.log("-DEBUG-selection:" + selection);
              console.log("-DEBUG-actionCount:" + actionCount);
              console.log("-DEBUG-gameSelected:" + gameSelected);

              if (
                game == "4D" &&
                selection == "sequence" &&
                actionCount > 0 &&
                isStoreResult
              ) {
                var tempData = storedResults[game][gameSelected];
                //store passed consolation/special
                console.log("sequence flow detected and stored past results");
                // console.log(tempData)
                var tempArray = [];
                for (var i in tempData) {
                  tempArray.push(tempData[i]);
                }
                tempArray.push(result[gameSelected][selectedSeq]);
                console.log(tempArray);
                storedResults[game][gameSelected] = tempArray;

                //store sequence
                var tempData = storedResults[game][selection];
                var tempArray = [];
                for (var i in tempData) {
                  tempArray.push(tempData[i]);
                }
                // console.log('pushed sequence ' + (actionCount-1) + " :: " + result[selection][actionCount-1])
                tempArray.push(result[selection][actionCount - 1]);
                storedResults[game][selection] = tempArray;
                isStoreResult = false;
              } else if (
                game == "jackpot" &&
                selection == "sequence" &&
                actionCount > 0
              ) {
                var tempData = storedResults[game][gameSelected];
                //store passed consolation/special
                console.log(
                  "jackpot sequence flow detected and stored past results"
                );
                // console.log(tempData)
                var tempArray = [];
                for (var i in tempData) {
                  tempArray.push(tempData[i]);
                }
                tempArray.push(actionData);
                // console.log(tempArray)
                storedResults[game][gameSelected] = tempArray;
              } else if (selection != "sequence" && actionCount > 0) {
                storedResults[game][gameSelected] = actionData;
              }
              if (game == "INTRO" || game == "OUTRO") {
                actionCount += 1;
                //force exit here
              }
              if (actionCount < actionSize) {
                console.log(selection + " : " + actionCount);
                if (actionSize == 1) {
                  // console.log(result[selection])
                  //all first prize or one time shuffle is here
                  actionData = result[selection];
                  // if(game == "jackpot") {
                  //   actionData = result[selection]
                  //   actionData = 'X' + actionData
                  // }
                } else {
                  // console.log(result[selection][actionCount])
                  if (
                    game == "4D" &&
                    selection == "sequence" &&
                    seqState == 0
                  ) {
                    console.log(
                      "sequence selected alphabet with count = " + actionCount
                    );
                    actionData = result[selection][actionCount];
                    for (var i in result["consolation_prize"]) {
                      // console.log(result['consolation_prize'][i][0])
                      if (actionData == result["consolation_prize"][i][0]) {
                        actionData = result["consolation_prize"][i];
                        gameSelected = "consolation_prize";
                        selectedSeq = i;
                      }
                    }
                    for (var i in result["special_prize"]) {
                      // console.log(result['special_prize'][i][0])
                      if (actionData == result["special_prize"][i][0]) {
                        actionData = result["special_prize"][i];
                        gameSelected = "special_prize";
                        selectedSeq = i;
                      }
                    }
                    //tune duration for 4d
                    gamePerActionDuration = 45;
                    //prepare for next state
                    prevState = 0;
                    seqState += 1;
                  } else if (
                    game == "4D" &&
                    selection == "sequence" &&
                    seqState == 1
                  ) {
                    console.log(
                      "sequence selected number with count = " + actionCount
                    );
                    //tune duration for 23abc
                    if (actionCount == 0) gamePerActionDuration = 22;
                    else gamePerActionDuration = 15;
                    //prepare for next actiom
                    actionCount += 1;
                    isStoreResult = true;
                    // gameSelected = 'sequence'
                    prevState = seqState;
                    seqState = 0;
                  } else {
                    //other game, which no need sequence
                    prevState = 1;
                    // console.log('other game which is not sequence')
                    console.log("-DEBUG- other flow game: " + game);
                    console.log("-DEBUG- other flow selection: " + selection);
                    if (game == "jackpot" && selection == "sequence") {
                      // console.log('game data of jackpot is selected')
                      selectedSeq = i;
                      gameSelected = sequence[actionCount].selection;
                      actionData = sequence[actionCount].data;
                    } else {
                      actionData = result[selection][actionCount];
                    }
                    //prepare for next actiom
                    actionCount += 1;
                  }
                }
                console.log("final action data: " + actionData);
                //action count is already increment during 2nd motion
                if (selection != "sequence") {
                  actionCount += 1;
                }
                // if(selection != 'sequence' && actionCount > 0) {
                //   storedResults[game][selection] = actionData
                // }
              } else {
                if (gameCount < gameSize) {
                  state = "PREPARE_GAME";
                  gameCount += 1;
                } else {
                  state = "END";
                  console.log("go to : " + state);
                }
              }
              count = 0;
              // console.log('Time count reset to : ' + count);
              // storedResult.push(storedData)
            }
            //if load next game state, dont admit anything
            if (state == "LOOP_NUMBER") {
              socket.emit("result", {
                data: actionData,
                selection: gameSelected,
                action_count: actionCount,
                action_size: actionSize,
                game: gameCount,
                game_name: game,
                progress: count,
                duration: gamePerActionDuration,
                sequence_state: prevState,
                stored_result: storedResults
              });
            }
          }
          if (state == "END") {
            getLive.status = 3;
            await getLive.save();
            liveResultThread.cancel();
            socket.emit("status", { date: now, state: "end" });
            return;
          }
          count += 1;
          // socket.emit('result',{
          //   'progress': count
          // })
        }
      );

      return response.status(200).send({
        success: true,
        message: "Live socket started",
        date: now
      });
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = LiveResultController;
