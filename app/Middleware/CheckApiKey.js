'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class CheckApiKey {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request ,response}, next) {
    if(request.input('api_key') != 'b3ed4a8b-06b7-4e08-9d54-656570b736b1') {
      return response.status('400').send({
        "success": false,
        'message': 'API key is wrong'
      })
    }
    // call next to advance the request
    await next()
  }
}

module.exports = CheckApiKey
