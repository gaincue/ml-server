'use strict'

/*
|--------------------------------------------------------------------------
| Websocket
|--------------------------------------------------------------------------
|
| This file is used to register websocket channels and start the Ws server.
| Learn more about same in the official documentation.
| https://adonisjs.com/docs/websocket
|
| For middleware, do check `wsKernel.js` file.
|
*/

// const Ws = use('Ws')
// Ws.channel('live', 'LiveStateController')

const Server = use('Server')
// console.log(Server.getInstance())
const io = use('socket.io')(Server.getInstance())
var socketDict = new Object()
// server:
io.on('connection', (socket) => {
  // socket.emit('transactionStatus','connected')
  console.log('connection established in socket.js')

  socket.on('result', (data) => {
    console.log('socket.js received: ' + JSON.stringify(data))
    socket.broadcast.emit('result',data)
    // console.log(name);
  });

  socket.on('status', (data) => {
    console.log('socket is : ' + JSON.stringify(data))
    socket.broadcast.emit('status',data)
    // console.log(name);
  });

  socket.on('disconnect', (data) => {
    console.log('socket detect disconnect')
  });
});


// Ws.channel('state', ({ socket }) => {
//   console.log('user joined with %s socket id', socket.id)
// })
