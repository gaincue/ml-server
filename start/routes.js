'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
Route.get('/','AdminController.index')
Route.get('/admin','AdminController.home')
Route.get("/qrcode/:key", "AdminController.showQRcode");
Route.post('/login','AdminController.login')
Route.post('/register','AdminController.postRegister')
Route.get('/run','LiveResultController.playLiveResult').middleware('checkApiKey')
Route.get('/result','LiveResultController.getResult').middleware('checkApiKey')
Route.get('/alternative_result','LiveResultController.getAlternativeResult').middleware('checkApiKey')
Route.get('/v2/alternative_result','LiveResultController.getAlternativeResultv2')
//getCurrentTime
Route.get('/time','LiveResultController.getCurrentTime').middleware('checkApiKey')
Route.get('/stoplive','AdminController.stopLive')
Route.get('/startlive','AdminController.startLive')
Route.get('/restartlive','AdminController.restartLive')
Route.get('/skiplive','AdminController.skipLive')
Route.delete('/result/:id','AdminController.deleteResult')
Route.get('/logout','AdminController.logout')
Route.get('/result/:date','LiveResultController.getResultPublic')
// Route.on('/').render('welcome')