const {hooks} = require('@adonisjs/ignitor')

hooks.after.providersBooted(() => {
    const View = use('View')    

    View.global('formatNumber', function (num) {
      return Number(0-num)
    })

    View.global('isEmpty', function (e) {    
      switch (e) {
        case "":
        case null:
        case false:
        case typeof this == "undefined":
          return true;
        default:
          return false;
      }
    })

    View.global('daysBetween',function(date1, date2) {
        var one_day=1000*60*60*24;    // Convert both dates to milliseconds
        var date1_ms = date1.getTime();   
        var date2_ms = date2.getTime();    // Calculate the difference in milliseconds  
        var difference_ms = date2_ms - date1_ms;        // Convert back to days and return   
        return Math.round(difference_ms/one_day); 
    })

    View.global('getName',function(key) {
      if (key == 'first_prize') {
        return "First Prize"
      } else if (key == 'second_prize') {
        return "Second Prize"
      } else if (key == 'third_prize') {
        return "Third Prize"
      } else if (key == 'consolation_prize') {
        return "Consolation Prize"
      } else if (key == 'special_prize') {
        return "Special Prize"
      } else if (key == '4D') {
        return "4D"
      } else if (key == '5D') {
        return "5D"
      } else if (key == 'jackpot') {
        return "Jackpot"
      } else if (key == '6D') {
        return "6D"
      }
  })
}) 