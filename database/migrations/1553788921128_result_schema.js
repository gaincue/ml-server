'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ResultSchema extends Schema {
  up () {
    this.create('results', (table) => {
      table.increments()
      table.string('date', 80).notNullable()
      table.integer('status').notNullable().defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('results')
  }
}

module.exports = ResultSchema
