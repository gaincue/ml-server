"use strict";
const User = use("App/Models/User");

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

class UserSeeder {
  async run() {
    const u1 = await User.create({
      email: "ml.co.hk88@gmail.com",
      password: "Abcd@1234",
      username: "mlhk88",
    });
  }
}

module.exports = UserSeeder;
