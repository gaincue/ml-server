'use strict'
const Result = use('App/Models/Result')

/*
|--------------------------------------------------------------------------
| ResultSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class ResultSeeder {
  async run () {
    const date = ['20190324','20190325','20190326','20190327','20190328','20190329','20190330','20190331',
      '20190401','20190402','20190403','20190404','20190405']

    for (var d in date) {
      await Result.create({
        date: date[d],
        status: 3
      })
    }
  }
}

module.exports = ResultSeeder
