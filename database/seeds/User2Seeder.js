"use strict";
const User = use("App/Models/User");

/*
|--------------------------------------------------------------------------
| User2Seeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

class User2Seeder {
  async run() {
    const u2 = await User.create({
      email: "city.admin@gmail.com",
      password: "City@8888",
      username: "city88",
    });
  }
}

module.exports = User2Seeder;
